
<?php

	include 'layout/header.php';

	include 'koneksi.php';

	$tanamankedelai = mysqli_query($koneksi, "SELECT * FROM kedelai");
	?>

	<h2 align="center"><font face="Fantasy"><b>Tabel Tanaman Kedelai</font></b></h2>
<br>


	<table class="table table-bordered w-100 p-3 ml-3">
	<thead class="bg-info">
		<tr>
			<th scope="col">no. urut</th>
			<th scope="col">waktutanamkedelai</th>
			<th scope="col">iklimkedelai</th>
			<th scope="col">datarankedelai</th>
			<th scope="col">perawatankedelai</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($tanamankedelai as $value):?>
		
		<tr>
			<th scope="row"><?php echo $value['kode']; ?></th>
			<th scope="row"><?php echo $value['waktutanamkedelai']; ?></th>
			<td><?php echo $value['iklimkedelai']; ?></td>
			<th scope="row"><?php echo $value['datarankedelai']; ?></th>
			<td><?php echo $value['perawatankedelai']; ?></td>
			<td>
				<a href="editkedelai.php?id=<?php echo $value['kode'] ?>" class="btn btn-primary btn-md">edit</a>
				<a href="hapuskedelai.php?id=<?php echo $value['kode'] ?>" class="btn btn-danger btn-md">Hapus</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="datakedelai.php" class="btn btn-primary btn-md">Tambah Data Kedelai</a>
<br><br>

	
<a href="projekta.php" class="btn btn-primary btn-md">MENU</a>