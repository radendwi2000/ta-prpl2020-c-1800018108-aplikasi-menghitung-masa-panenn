<?php

	include 'layout/header.php';

	include 'koneksi.php';

	$tanamanjagung = mysqli_query($koneksi, "SELECT * FROM jagung");
?>

	<h2 align="center"><font face="Fantasy"><b>Tabel Tanaman Jagung</font></b></h2>
<br>


	<table class="table table-bordered w-100 p-3 ml-3">
	<thead class="bg-info">
		<tr>
			<th scope="col">no. urut</th>
			<th scope="col">waktutanamjagung</th>
			<th scope="col">iklimjagung</th>
			<th scope="col">dataranjagung</th>
			<th scope="col">perawatanjagung</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($tanamanjagung as $value):?>
		
		<tr>
			<th scope="row"><?php echo $value['kode']; ?></th>
			<th scope="row"><?php echo $value['waktutanamjagung']; ?></th>
			<td><?php echo $value['iklimjagung']; ?></td>
			<th scope="row"><?php echo $value['dataranjagung']; ?></th>
			<td><?php echo $value['perawatanjagung']; ?></td>
			<td>
				<a href="editjagung.php?id=<?php echo $value['kode'] ?>" class="btn btn-primary btn-md">edit</a>
				<a href="hapusjagung.php?id=<?php echo $value['kode'] ?>" class="btn btn-danger btn-md">Hapus</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="datajagung.php" class="btn btn-primary btn-md">Tambah Data jagung</a>
<br><br>

	
<a href="projekta.php" class="btn btn-primary btn-md">HOME</a>