<?php

	include 'layout/header.php';

	include 'koneksi.php';

	$tanamankacang = mysqli_query($koneksi, "SELECT * FROM kacang tanah");
	?>

	<h2 align="center"><font face="Fantasy"><b>Tabel Tanaman Padi</font></b></h2>
<br>


	<table class="table table-bordered w-100 p-3 ml-3">
	<thead class="bg-info">
		<tr>
			<th scope="col">no. urut</th>
			<th scope="col">waktutanamkacang</th>
			<th scope="col">iklimkacang</th>
			<th scope="col">datarankacang</th>
			<th scope="col">perawatankacang</th>
			<th scope="col">masapanen</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($tanamankacang as $value):?>
		
		<tr>
			<th scope="row"><?php echo $value['kode']; ?></th>
			<th scope="row"><?php echo $value['waktutanamkacang']; ?></th>
			<td><?php echo $value['iklimkacang']; ?></td>
			<th scope="row"><?php echo $value['datarankacang']; ?></th>
			<td><?php echo $value['perawatankacang']; ?></td>
			<td><?php echo $value['perhitungan']; ?></td>
			<td>
				<a href="editkacang.php?id=<?php echo $value['kode'] ?>" class="btn btn-primary btn-md">edit</a>
				<a href="hapuskacang.php?id=<?php echo $value['kode'] ?>" class="btn btn-danger btn-md">Hapus</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="datakacang.php" class="btn btn-primary btn-md">Tambah Data Padi</a>
<br><br>

	
<a href="projekta.php" class="btn btn-primary btn-md" align="center">    HOME    </a>