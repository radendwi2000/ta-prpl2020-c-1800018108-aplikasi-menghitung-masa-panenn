<?php

include 'koneksi.php';

?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="">
    
    <style>
    body{
    background-image: url(sawah.jpg);
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    font-family: monospace;
    background-size: cover;
}
.boxlogin{
    width: 320px;
    height: 520px;
    background:rgb(0,0, 0, .7);
    color: #fff;
    top: 50%;
    left: 50%;
    position: absolute;
    transform: translate(-50%,-50%);
    box-sizing: border-box;
    border-radius: 20px;
    padding: 50px 30px;
}
h1{
    margin: 0;
    padding: 0 0 20px;
    text-align: center;
    font-size: 22px;
}
h2{
    margin: 0;
    padding: 0 0 20px;
    text-align: center;
    font-size: 22px;
}
h4{
    margin: 0;
    padding: 0 0 20px;
    font-size: 14px;
  
}
.boxlogin p{
    margin: 0;
    padding: 0;
    font-weight: bold;
    padding: 0;
}
.boxlogin input{
    width: 100%;
    margin-bottom: 20px;
}
.boxlogin input[type="text"],input[type="password"]{
    border: none;
    border-bottom: 1px solid #fff;
    background: transparent;
    outline: none;
    height: 40px;
    color: #fff;
    font-size: 16px;
}
.boxlogin input[type="submit"]{
    border: none;
    outline: none;
    height: 40px;
    background: rgb(66,197,244,.7);
    color: black;
    font-size: 18px;
    border-radius: 20px;
}

.boxlogin a {
color: #fff;
}
 </style>
</head>
<body>
    </div>
    <div class="boxlogin">
        <h1>MASUKAN DATA</h1>
        <h4>*isilah data dengan kondisi tanaman anda</h4>
        <form method="post" action="proseskacang.php">
        	<input type="text" name="kode" id="kode" placeholder="Nomor urutan">
            <input type="text" name="waktutanamkacang" id="waktutanamkacang" placeholder="Waktu Tanam">
            <input type="text" name="iklimkacang" id="iklimkacang" placeholder="Iklim">
            <input type="text" name="datarankacang" id="datarankacang" placeholder="permukaan dataran tanam">
            <input type="text" name="perawatankacang" id="perawatakacang" placeholder="Pupuk">
            <input type="submit" name="daftar" value="HITUNG">
        </form>
        <a href="projekta.php" id="home">
            <h2>MENU</h2>
        </a>
    </div>
</body>
</html>