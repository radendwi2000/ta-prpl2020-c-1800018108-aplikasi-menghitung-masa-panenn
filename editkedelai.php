<?php 

	include 'layout/header.php';

	include 'koneksi.php';


	$id = $_GET['id'];
	$tanamankedelai = mysqli_query($koneksi, "SELECT * FROM kedelai where kode = '$id'");

	foreach($tanamankedelai as $value):
?>


<h2>Ubah Data Tanaman Kedelai</h2>

<form method="post" action="ubahkedelai.php" class="w-50 p-3">
	<div class="form-group row">
		<label for="formGroupExampleInput" class="col-sm-2 col-form-label">no. urut</label>
		<div class="col-sm-10">
			<input type="text" readonly class="form-control" id="formGroupExampleInput" name="kode" value="<?php echo $value['kode'] ?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="formGroupExampleInput" class="col-sm-2 col-form-label">waktutanamkedelai</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="formGroupExampleInput" name="waktutanamkedelai" value="<?php echo $value['waktutanamkedelai'] ?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="formGroupExampleInput" class="col-sm-2 col-form-label">iklimkedelai</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="formGroupExampleInput" name="iklimkedelai" value="<?php echo $value['iklimkedelai'] ?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="formGroupExampleInput" class="col-sm-2 col-form-label">datarankedelai</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="formGroupExampleInput" name="datarankedelai" value="<?php echo $value['datarankedelai'] ?>">
		</div>
	</div>
	<div class="form-group row">
		<label for="formGroupExampleInput" class="col-sm-2 col-form-label">perawatankedelai</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="formGroupExampleInput" name="perawatankedelai" value="<?php echo $value['perawatankedelai'] ?>">
		</div>
	</div>
	<button name="tambah" class="btn btn-primary btn-md"> Ubah</button>
</form>

<?php 
	
	endforeach;
	include 'layout/footer.php';

 ?>