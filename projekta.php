<?php

include 'layout/header.php';

include 'koneksi.php';

?>
<head>
  <title>MENU</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <style>
        body{
            background-color: #ffffcc;
           width: 100%;
        }
        
        .atas{
            margin-top: 20px;
            width: 100%;
            
        }
       
        table h1 {
            padding-left: 30px;
            font-family:Georgia, 'Times New Roman', Times, serif;
        }

        table h4{
            padding-left: 30px;
            font-family:Georgia, 'Times New Roman', Times, serif;
        }

        .mid{
            width: 100%;
            text-align: center;
            float: left;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        
  
        .footer{
            width: 100%;
            float: right;
            text-align: center;
            margin-top: 20px;
            background-color: ;
        }

        .footer p{
            padding-top: 10px;
        }
        table{
            background-color: #e6ffb3;
        }
        table img{
            padding-left: 80px;
            padding-right: 40px;
            margin-left:250px;
        }

        .btn{
            padding-bottom: 15px;
        }

        .btn h4{
          padding-bottom: 1px;
        }
    </style>
</head>
<body>
        <div class="atas">
          <table border="0px">
              <tr>
                  <td>
                                <h1>Selamat Datang di Website Perhitungan Masa Panen</h1>
                                <h4>Website mempermudah anda memperkirakan masa panen tanaman anda di Sawah</h4>     
                  </td>
                  <td>
                        <img src="logohome.png" alt="" width="250px">
                  </td>
              </tr>
          </table>       
        </div>
    
        <div class="mid">          
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="1"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                          <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                      
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <div class="item active">
                            <img src="fotopadii.jpg" alt="gambar1">
                          </div>
                      
                          <div class="item">
                            <img src="fotokacangg.jpg" alt="gambar2">
                          </div>
                        
                          <div class="item">
                            <img src="fotojagungg.jpg" alt="gambar3">
                          </div>
                        
                        <div class="item">
                            <img src="fotokedelaii.jpg" alt="gambar4">
                        </div>
                            
                        </div>
                      
                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                      
    </div>
        <div class="btn-group btn-group-justified">
                <div class="btn-group">
                 <button type="button" class="btn">
                   <div class="item">
                            <img src="logopadii.png" alt="logo1">
                        </div>     
                  <a href="datapadi.php"><h4>Klik Disini!</h4></a>
                </button> 
                </div>

                <div class="btn-group">
                  <button type="button" class="btn">
                    <div class="item">
                            <img src="logojagungg.png" alt="logo1">
                        </div> 
                    <a href="datajagung.php"><h4>Klik Disini!</h4></a>
                  </button>
                </div>

                <div class="btn-group">
                  <button type="button" class="btn">
                    <div class="item">
                            <img src="logokacangg.png" alt="logo1">
                        </div> 
                    <a href="datakacang.php"><h4>Klik Disini!</h4></a>
                  </button>
                </div>

                <div class="btn-group">
                    <button type="button" class="btn">
                    <div class="item">
                        <img src="logokedelaiii.png" alt="logo1">
                    </div> 
                    <a href="datakedelai.php"><h4>Klik Disini!</h4></a>
                    </button>
                </div>
         </div>
              
                <div class="footer">
                    <p><b>&copy HatiTani</b></p>
                </div>
    
    </body>
</html>
</html>