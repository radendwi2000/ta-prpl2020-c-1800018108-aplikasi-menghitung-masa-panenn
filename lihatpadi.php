<?php

	include 'layout/header.php';

	include 'koneksi.php';

	$tanamanpadi = mysqli_query($koneksi, "SELECT * FROM padi");
	?>

	<h2 align="center"><font face="Fantasy"><b>Tabel Tanaman Padi</font></b></h2>
<br>


	<table class="table table-bordered w-100 p-3 ml-3">
	<thead class="bg-info">
		<tr>
			<th scope="col">no. urut</th>
			<th scope="col">waktutanampadi</th>
			<th scope="col">iklimpadi</th>
			<th scope="col">dataranpadi</th>
			<th scope="col">perawatanpadi</th>
			<th scope="col">masapanen</th>
			<th scope="col">Action</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($tanamanpadi as $value):?>
		
		<tr>
			<th scope="row"><?php echo $value['kode']; ?></th>
			<th scope="row"><?php echo $value['waktutanampadi']; ?></th>
			<td><?php echo $value['iklimpadi']; ?></td>
			<th scope="row"><?php echo $value['dataranpadi']; ?></th>
			<td><?php echo $value['perawatanpadi']; ?></td>
			<td><?php echo $value['perhitungan']; ?></td>
			<td>
				<a href="editpadi.php?id=<?php echo $value['kode'] ?>" class="btn btn-primary btn-md">edit</a>
				<a href="hapuspadi.php?id=<?php echo $value['kode'] ?>" class="btn btn-danger btn-md">Hapus</a>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<a href="datapadi.php" class="btn btn-primary btn-md">Tambah Data Padi</a>
<br><br>

	
<a href="projekta.php" class="btn btn-primary btn-md" align="center">    HOME    </a>